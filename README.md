# Development Setup

- Install docker, docker-compose, npm, lerna
- create `.env` file for docker-compose at root of repo

```
DB_USER=frappe-client-registry
DB_PASSWORD=admin
DB_NAME=frappe-client-registry
MONGODB_ROOT_PASSWORD=admin
```

- create `.env` file for for server on /package/frappe-client-registry/
```
DB_USER=frappe-client-registry
DB_PASSWORD=admin
DB_NAME=frappe-client-registry
DB_HOST=localhost
```


- start docker backing services (from repo root)

```sh
docker-compose --project-name dtisupport -f docker/docker-compose.yml up -d
```

- bootstrap dependencies (from repo root)

```sh
lerna clean && lerna bootstrap
```

In VSCode

- To start server: C+Shift+P > Run Task > start:debug
- Wait a minute or two for server start before starting client.
- To start client: C+Shift+P > Run Task > start

### Setup Dev Server

- Login to the staging authorization server and select the create or select a trusted client for support portal
- Make a Http `POST` request to http://support.localhost:7000/api/setup with following body params
    - appURL - http://support.localhost:7000
    - authServerURL - https://staging-accounts.castlecraft.in
    - clientId: select from authorization server
    - clientSecret: select from authorization server
