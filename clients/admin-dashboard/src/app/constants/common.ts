export const NEW_ID: string = 'new';
export const MODELS = [
  { route: 'client' },
  { route: 'role' },
  { route: 'user' },
  { route: 'scope' },
];
export const DURATION = 5000;
export const UNDO_DURATION = 3000;
export const SCOPE = 'openid email roles profile';
