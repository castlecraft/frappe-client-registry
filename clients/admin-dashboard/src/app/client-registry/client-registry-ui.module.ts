import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedImportsModule } from '../shared-imports/shared-imports.module';
import { ClientRegistrySettingsComponent } from './client-registry-settings/client-registry-settings.component';
import { ClientRegistrySettingsService } from './client-registry-settings/client-registry-settings.service';
import { RoleComponent } from './role/role.component';
import { RoleService } from './role/role.service';
import { UserComponent } from './user/user.component';
import { ClientComponent } from './client/client.component';
import { ClientRoleComponent } from './client-role/client-role.component';
import { ClientRoleService } from './client-role/client-role.service';
import { FrappeInstanceComponent } from './client/frappe-instance/frappe-instance.component';

@NgModule({
  declarations: [
    ClientComponent,
    ClientRegistrySettingsComponent,
    ClientRoleComponent,
    RoleComponent,
    UserComponent,
    FrappeInstanceComponent,
  ],
  imports: [CommonModule, SharedImportsModule],
  exports: [
    ClientRegistrySettingsComponent,
    RoleComponent,
    UserComponent,
    ClientComponent,
    ClientRoleComponent,
  ],
  providers: [ClientRegistrySettingsService, RoleService, ClientRoleService],
})
export class ClientRegistryUIModule {}
