import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FrappeInstanceComponent } from './frappe-instance.component';
import { MaterialModule } from '../../../shared-imports/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpErrorHandler } from '../../../common/services/http-error-handler/http-error-handler.service';
import { MessageService } from '../../../common/services/message/message.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { oauthServiceStub } from '../../../common/testing-helpers';
import { ClientService } from '../client.service';

describe('FrappeInstanceComponent', () => {
  let component: FrappeInstanceComponent;
  let fixture: ComponentFixture<FrappeInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FrappeInstanceComponent],
      imports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes([]),
        BrowserAnimationsModule,
      ],
      providers: [
        HttpErrorHandler,
        MessageService,
        {
          provide: OAuthService,
          useValue: oauthServiceStub,
        },
        {
          provide: ClientService,
          useValue: {
            getUuid: (...args) => {},
          },
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrappeInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
