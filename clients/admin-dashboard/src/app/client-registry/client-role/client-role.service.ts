import { Injectable } from '@angular/core';
import {
  HandleError,
  HttpErrorHandler,
} from '../../common/services/http-error-handler/http-error-handler.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CANNOT_FETCH_CLIENT } from '../../constants/messages';
import { OAuthService } from 'angular-oauth2-oidc';
import {
  ADD_USER,
  CREATE_CLIENT,
  GET_CLIENT_ROLE,
  DELETE_USER,
} from '../../../app/constants/url-paths';

@Injectable({
  providedIn: 'root',
})
export class ClientRoleService {
  private handleError: HandleError;
  authorizationHeader: HttpHeaders;

  constructor(
    httpErrorHandler: HttpErrorHandler,
    private readonly http: HttpClient,
    private readonly oauthService: OAuthService,
  ) {
    this.handleError = httpErrorHandler.createHandleError('UserService');
  }

  getClientRole(userID: string): Observable<any> {
    const url = GET_CLIENT_ROLE + `/${userID}`;
    return this.http
      .get<string>(url, { headers: this.getHeaders() })
      .pipe(
        catchError(
          this.handleError('getClientRole', { message: CANNOT_FETCH_CLIENT }),
        ),
      );
  }

  createClientRole(
    clientUuid: string,
    role: string,
    users: string[],
    frappeClientUuid: string,
  ) {
    const url = CREATE_CLIENT;
    const clientRoleData = {
      clientUuid,
      name: role,
      users,
      frappeClientUuid,
    };

    return this.http.post(url, clientRoleData, { headers: this.getHeaders() });
  }

  addUser(uuid: string, users: string[]) {
    const url = ADD_USER;
    const clientRoleData: any = {
      uuid,
      users,
    };

    return this.http.post(url, clientRoleData, { headers: this.getHeaders() });
  }

  deleteClientRole(uuid, users: string[]) {
    return this.http.post(
      DELETE_USER,
      { uuid, users },
      { headers: this.getHeaders() },
    );
  }

  getHeaders() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }
}
