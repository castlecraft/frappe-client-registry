import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  MatSnackBar,
  MatAutocomplete,
  MatAutocompleteSelectedEvent,
  MatChipInputEvent,
} from '@angular/material';
import { NEW_ID, DURATION, UNDO_DURATION } from '../../constants/common';
import { ClientRoleService } from './client-role.service';
import {
  CREATE_SUCCESSFUL,
  CLOSE,
  CREATE_ERROR,
  UPDATE_SUCCESSFUL,
  UPDATE_ERROR,
  DELETING,
  UNDO,
} from '../../constants/messages';
import { RoleService } from '../role/role.service';
import { ListingService } from '../../../app/shared-ui/listing/listing.service';
import { map, debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ClientService } from '../client/client.service';

export const CLIENT_ROLE_LIST_ROUTE = '/list/client-role';

@Component({
  selector: 'app-user',
  templateUrl: './client-role.component.html',
  styleUrls: ['./client-role.component.css'],
})
export class ClientRoleComponent implements OnInit {
  uuid: string;
  userEmail: string;
  roles: string[] = [];
  hide: boolean = true;
  flagDeleteUser: boolean = false;
  clients;
  role;
  search;
  frappeInstances;
  selectedClientUuid;

  ///
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: any[] = [','];
  usersControl = new FormControl();
  filteredUsers: Observable<string[]>;
  users: string[] = [];
  addUsers: string[] = [];

  @ViewChild('usersInput', { static: false }) fruitInput: ElementRef<
    HTMLInputElement
  >;
  @ViewChild('auto2', { static: false }) matAutocomplete: MatAutocomplete;

  clientRoleForm: FormGroup = new FormGroup({
    uuid: new FormControl(this.uuid),
    role: new FormControl(''),
    userEmail: new FormControl(this.userEmail),
    client: new FormControl(''),
    clientUuid: new FormControl(''),
    frappeInstance: new FormControl(''),
    frappeInstanceUuid: new FormControl(''),
  });

  constructor(
    private readonly clientRoleService: ClientRoleService,
    private readonly roleService: RoleService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private router: Router,
    private readonly listingService: ListingService,
    private readonly clientService: ClientService,
  ) {
    this.uuid =
      this.route.snapshot.params.uuid === NEW_ID
        ? null
        : this.route.snapshot.params.uuid;
  }

  ngOnInit() {
    if (this.uuid && this.uuid !== NEW_ID) {
      this.subscribeGetClientRole(this.uuid);
    } else {
      this.filteredUsers = this.subscribeGetUsers();
      this.subscribeGetRoles();
      this.subscribeListClients();
    }
  }

  subscribeGetClientRole(uuid: string) {
    this.clientRoleService.getClientRole(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateClientRoleForm(response);
        }
      },
    });
  }

  subscribeGetRoles() {
    this.roleService.getRoles().subscribe({
      next: (response: any) => {
        if (response) {
          response.docs.map(role => {
            this.roles.push(role.name);
          });
        }
      },
    });
  }

  createUser() {
    this.clientRoleService
      .createClientRole(
        this.clientRoleForm.controls.clientUuid.value,
        this.clientRoleForm.controls.role.value,
        this.users,
        this.clientRoleForm.controls.frappeInstanceUuid.value,
      )
      .subscribe({
        next: success => {
          this.snackBar.open(CREATE_SUCCESSFUL, CLOSE, { duration: DURATION });
          this.router.navigateByUrl(CLIENT_ROLE_LIST_ROUTE);
        },
        error: err =>
          this.snackBar.open(CREATE_ERROR, CLOSE, { duration: DURATION }),
      });
  }

  updateUser() {
    this.clientRoleService.addUser(this.uuid, this.addUsers).subscribe({
      next: success => {
        this.snackBar.open(UPDATE_SUCCESSFUL, CLOSE, { duration: DURATION });
        this.router.navigateByUrl(CLIENT_ROLE_LIST_ROUTE);
      },
      error: err => {
        this.snackBar.open(UPDATE_ERROR, CLOSE, { duration: DURATION });
      },
    });
  }

  userKeyUp() {
    this.filteredUsers = this.subscribeGetUsers(this.usersControl.value);
  }

  addClient(selectedClient) {
    this.selectedClientUuid = selectedClient.uuid;
    this.clientRoleForm.controls.clientUuid.setValue(selectedClient.uuid);
    if (!selectedClient.frappeInstance) {
      this.snackBar.open('client has no frappe Instance', CLOSE, {
        duration: 1000,
      });
      this.clientRoleForm.controls.frappeInstance.disable();
    } else {
      this.frappeInstanceList();
    }
  }

  addFrappeInstance(selectedFrappeInstance) {
    this.clientRoleForm.controls.frappeInstanceUuid.setValue(
      selectedFrappeInstance.frappeClientUuid,
    );
  }

  subscribeGetUsers(search?) {
    return this.listingService.findModels('user', search, '', 0, 10).pipe(
      map((resp: any) => {
        if (resp.docs) {
          return resp.docs.map(client => client.email);
        }
      }),
      debounceTime(1000),
    );
  }

  searchKeyUp() {
    this.clientRoleForm.controls.client.valueChanges
      .pipe(debounceTime(1000))
      .subscribe({
        next: success => {
          this.search = success;
          this.subscribeListClients();
        },
      });
  }

  searchFrappeInstance() {
    this.clientRoleForm.controls.frappeInstance.valueChanges
      .pipe(debounceTime(1000))
      .subscribe({
        next: success => {
          this.frappeInstanceList();
        },
      });
  }

  subscribeListClients() {
    this.clients = this.listingService
      .findModels('client', this.search, '', 0, 10)
      .pipe(
        map((resp: any) => {
          if (resp.docs) {
            return resp.docs.map(client => client);
          }
        }),
        debounceTime(1000),
      );
  }

  frappeInstanceList() {
    this.frappeInstances = this.clientService
      .getClient(this.selectedClientUuid)
      .pipe(
        map((resp: any) => {
          if (resp) {
            return resp[0].frappeInstance.map(frappeInstance => {
              return frappeInstance;
            });
          }
        }),
        debounceTime(1000),
      );
  }

  populateClientRoleForm(user) {
    this.userEmail = user.email;
    this.uuid = user.uuid;
    this.clientRoleForm.controls.client.setValue(user.clientUuid);
    this.clientRoleForm.controls.frappeInstance.setValue(user.frappeClientUuid);
    this.role = user.name;
    this.clientRoleForm.controls.role.setValue(user.name);
    this.users = user.users;
    this.clientRoleForm.controls.client.disable();
    this.clientRoleForm.controls.role.disable();
  }

  add(event: MatChipInputEvent): void {
    if (!this.users.includes(event.value)) {
      return;
    }
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      if ((value || '').trim()) {
        this.users.push(value.trim());
      }

      if (input) {
        input.value = '';
      }

      this.usersControl.setValue(null);
    }
  }

  remove(user: string): void {
    const index = this.users.indexOf(user);
    const updateIndex = this.addUsers.indexOf(user);
    this.users.splice(index, 1);
    if (index >= 0) {
      this.flagDeleteUser = true;
      const snackBar = this.snackBar.open(DELETING, UNDO, {
        duration: UNDO_DURATION,
      });

      snackBar.afterDismissed().subscribe({
        next: dismissed => {
          if (this.flagDeleteUser) {
            this.clientRoleService
              .deleteClientRole(this.uuid, [user])
              .subscribe({
                next: deleted => {},
                error: err => {},
              });
          }
        },
        error: err => {},
      });

      snackBar.onAction().subscribe({
        next: success => {
          this.flagDeleteUser = false;
          this.users.splice(index, 0, user);
        },
        error: err => {},
      });
    }
    if (updateIndex >= 0) {
      this.addUsers.splice(updateIndex, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.users.includes(event.option.viewValue)) {
      return;
    }
    if (this.uuid !== NEW_ID) {
      this.addUsers.push(event.option.viewValue);
    }
    this.users.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.usersControl.setValue(null);
  }
}
