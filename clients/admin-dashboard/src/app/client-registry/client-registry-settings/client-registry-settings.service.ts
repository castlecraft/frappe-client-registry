import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { ISSUER_URL } from '../../constants/storage';

@Injectable({
  providedIn: 'root',
})
export class ClientRegistrySettingsService {
  constructor(private http: HttpClient, private oauthService: OAuthService) {}

  getSettings() {
    return this.http.get('/api/settings/v1/get', {
      headers: this.getHeaders(),
    });
  }

  updateSettings(appURL: string, clientId: string, clientSecret: string) {
    const authServerURL = localStorage.getItem(ISSUER_URL);
    return this.http.post(
      'api/settings/v1/update',
      {
        authServerURL,
        appURL,
        clientId,
        clientSecret,
      },
      { headers: this.getHeaders() },
    );
  }

  getHeaders() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }

  setupPermission() {
    return this.http.post(
      'api/settings/v1/setup_permission_sync',
      {},
      { headers: this.getHeaders() },
    );
  }
}
