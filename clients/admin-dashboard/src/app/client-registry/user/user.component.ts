import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NEW_ID, DURATION } from '../../constants/common';
import { UserService } from './user.service';
import { MatSnackBar } from '@angular/material';
import { CLOSE } from '../../../app/constants/messages';

export const USER_LIST_ROUTE = '/list/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  uuid: string;
  name: string;
  email: string;
  hide: boolean = true;

  userForm: FormGroup = new FormGroup({
    name: new FormControl(this.name),
    uuid: new FormControl(this.uuid),
    email: new FormControl(this.email),
  });

  constructor(
    private readonly userService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {}

  ngOnInit() {
    if (
      !this.route.snapshot.params.uuid ||
      this.route.snapshot.params.uuid === NEW_ID
    ) {
      this.router.navigateByUrl(USER_LIST_ROUTE);
    }
    this.uuid = this.route.snapshot.params.uuid;
    if (this.uuid && this.uuid !== NEW_ID) {
      this.subscribeGetUser(this.uuid);
    }
  }

  subscribeGetUser(uuid: string) {
    this.userService.getUser(uuid).subscribe({
      next: response => {
        if (response) {
          this.populateUserForm(response);
        }
      },
      error: error =>
        this.snackBar.open(error.error.messages, CLOSE, { duration: DURATION }),
    });
  }

  populateUserForm(user) {
    this.name = user.name;
    this.email = user.email;
    this.uuid = user.uuid;
    this.userForm.controls.name.setValue(user.name);
    this.userForm.controls.email.setValue(user.email);
  }
}
