import { Injectable } from '@angular/core';
import {
  HandleError,
  HttpErrorHandler,
} from '../../common/services/http-error-handler/http-error-handler.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CANNOT_FETCH_CLIENT } from '../../constants/messages';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root',
})
export class RoleService {
  private handlerror: HandleError;
  constructor(
    private readonly http: HttpClient,
    httpErrorHandler: HttpErrorHandler,
    private oauthService: OAuthService,
  ) {
    this.handlerror = httpErrorHandler.createHandleError('RoleService');
  }

  createRole(roleName: string) {
    const url = `api/role/v1/create`;
    const clientData = {
      name: roleName,
    };
    return this.http.post(url, clientData, {
      headers: this.getHeaders(),
    });
  }

  getRole(role: string): Observable<any> {
    const url = `api/role/v1/get/${role}`;
    return this.http
      .get<string>(url, { headers: this.getHeaders() })
      .pipe(
        catchError(
          this.handlerror('getRole', { message: CANNOT_FETCH_CLIENT }),
        ),
      );
  }

  updateRole(uuid: string, roleName: string) {
    const url = `api/role/v1/update`;
    const roleData = {
      name: roleName,
      uuid,
    };
    return this.http.post(url, roleData, {
      headers: this.getHeaders(),
    });
  }

  getRoles() {
    return this.http.get('api/role/v1/list', { headers: this.getHeaders() });
  }

  getHeaders() {
    return new HttpHeaders({
      Authorization: 'Bearer ' + this.oauthService.getAccessToken(),
    });
  }
}
