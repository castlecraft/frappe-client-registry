import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveUsersNotInCompanyQuery } from './retrieve-user-excluded-by-company.query';
import { UserAggregateService } from '../../aggregates/user-aggregate/user-aggregate.service';

@QueryHandler(RetrieveUsersNotInCompanyQuery)
export class RetrieveUsersNotInCompanyHandler
  implements IQueryHandler<RetrieveUsersNotInCompanyQuery> {
  constructor(private readonly manager: UserAggregateService) {}
  async execute(query: RetrieveUsersNotInCompanyQuery) {
    const { company, offset, limit, search, sort } = query;
    return await this.manager
      .getUserExcludedByCompany(
        company,
        Number(offset),
        Number(limit),
        search,
        sort,
      )
      .toPromise();
  }
}
