import { RetrieveUserHandler } from './get-user/retrieve-user.handler';
import { RetrieveUserListHandler } from './list-user/retrieve-user-list.handler';
import { RetrieveUsersNotInCompanyHandler } from './list-user-excluded-by-company/retrieve-user-excluded-by-company.handler';

export const QueryManager = [
  RetrieveUserHandler,
  RetrieveUserListHandler,
  RetrieveUsersNotInCompanyHandler,
];
