import { UserAggregateService } from './user-aggregate/user-aggregate.service';
import { FrappeUserPermissionAggregateService } from './frappe-user-permission-aggregate/frappe-user-permission-aggregate.service';

export const UserAggregateManager = [
  UserAggregateService,
  FrappeUserPermissionAggregateService,
];
