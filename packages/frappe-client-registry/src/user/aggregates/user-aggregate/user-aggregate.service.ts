import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { UserDto } from '../../entities/user/user.dto';
import * as uuidv4 from 'uuid/v4';
import { AggregateRoot } from '@nestjs/cqrs';
import { UserAddedEvent } from '../../events/user-added/user-added.event';
import { COMPANY_ROLE_NOT_FOUND } from '../../../constants/messages';
import { UserService } from '../../entities/user/user.service';
import { UserUpdatedEvent } from '../../events/user-updated/user-updated.event';
import { UpdateUserDto } from '../../entities/user/update-user.dto';
import { User } from '../../entities/user/user.entity';
import { UserRemovedEvent } from '../../events/user-removed/user-removed.event';
import { from, throwError, of } from 'rxjs';
import {
  switchMap,
  retryWhen,
  concat,
  delay,
  take,
  catchError,
  retry,
  mergeMap,
} from 'rxjs/operators';
import { ClientRoleService } from '../../../client-role/entity/client-role/client-role.service';
import { UserPoliciesService } from '../../../user/policies/user-policies/user-policies.service';

@Injectable()
export class UserAggregateService extends AggregateRoot {
  constructor(
    private readonly userService: UserService,
    private readonly clientRoleService: ClientRoleService,
    private readonly userPolicyService: UserPoliciesService,
  ) {
    super();
  }

  addNewUser(userPayload: UserDto, attempt: number, eventNumber?: number) {
    return of({}).pipe(
      switchMap(empty => {
        return this.retryAndCreateUser(userPayload);
      }),
      delay(this.generateRandomTime()),
      retry(Math.floor(Math.random() * attempt + 1)),
      catchError(error => {
        return this.createUserEvent(userPayload);
      }),
    );
  }

  retryAndCreateUser(userPayload: UserDto) {
    let randomTime;
    return of({}).pipe(
      switchMap(empty => {
        return this.userPolicyService.validateUser(userPayload).pipe(
          switchMap(existingUser => {
            return of({});
          }),
        );
      }),
      retryWhen(errors => {
        return errors.pipe(
          mergeMap(err => {
            randomTime = this.generateRandomTime();
            return of(err).pipe(delay(randomTime));
          }),
          take(Math.floor(Math.random() * 6)),
          concat(throwError(errors)),
        );
      }),
    );
  }

  createUserEvent(userPayload) {
    const user = Object.assign(new User(), userPayload);
    user.uuid = uuidv4();
    this.apply(new UserAddedEvent(user));
    return of({});
  }

  async removeUser(uuid: string) {
    const user = await this.userService.findOne({ uuid });
    if (!user) {
      throw new NotFoundException();
    }
    this.apply(new UserRemovedEvent(user));
  }

  updateUser(updatePayload: UpdateUserDto) {
    return this.userPolicyService.validateExistingUser(updatePayload).pipe(
      switchMap(existingUser => {
        existingUser.name = updatePayload.name;
        this.apply(new UserUpdatedEvent(existingUser));
        return of({});
      }),
    );
  }

  async retrieveUser(uuid: string, clientHttpRequest) {
    const provider = await this.userService.findOne({ uuid });
    if (!provider) {
      throw new NotFoundException();
    }
    return provider;
  }

  async getUserList(offset, limit, search, sort) {
    return this.userService.list(offset, limit, search, sort);
  }
  getUserExcludedByCompany(company, offset, limit, search, sort) {
    return from(this.clientRoleService.findOne({ uuid: company })).pipe(
      switchMap(clientRole => {
        if (!clientRole) {
          return throwError(new BadRequestException(COMPANY_ROLE_NOT_FOUND));
        }
        const clientRoleUsers = clientRole.users || [];
        return this.userService.listByFilter(
          clientRoleUsers,
          offset,
          limit,
          search,
          sort,
        );
      }),
    );
  }

  generateRandomTime() {
    return Number(((Math.random() * 10) % 3) * 1000);
  }
}
