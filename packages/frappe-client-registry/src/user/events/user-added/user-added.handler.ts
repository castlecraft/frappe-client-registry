import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserAddedEvent } from './user-added.event';
import { UserService } from '../../entities/user/user.service';

@EventsHandler(UserAddedEvent)
export class UserAddedHandler implements IEventHandler<UserAddedEvent> {
  constructor(private readonly addUser: UserService) {}
  async handle(event: UserAddedEvent) {
    const { user } = event;
    await this.addUser.create(user);
  }
}
