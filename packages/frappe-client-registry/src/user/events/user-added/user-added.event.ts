import { IEvent } from '@nestjs/cqrs';
import { User } from '../../entities/user/user.entity';

export class UserAddedEvent implements IEvent {
  constructor(public user: User) {}
}
