import { UserAddedHandler } from './user-added/user-added.handler';
import { UserUpdatedHandler } from './user-updated/user-updated.handler';
import { UserRemovedHandler } from './user-removed/user-removed.handler';

export const EventManager = [
  UserAddedHandler,
  UserUpdatedHandler,
  UserRemovedHandler,
];
