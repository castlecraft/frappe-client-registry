import { ICommand } from '@nestjs/cqrs';
import { UserDto } from '../../entities/user/user.dto';

export class AddNewUserCommand implements ICommand {
  constructor(
    public userPayload: UserDto,
    public retry: number,
    public eventNumber?: number,
  ) {}
}
