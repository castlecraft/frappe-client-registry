import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddNewUserCommand } from './add-new-user.command';
import { UserAggregateService } from '../../aggregates/user-aggregate/user-aggregate.service';

@CommandHandler(AddNewUserCommand)
export class AddNewUserHandler implements ICommandHandler<AddNewUserCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: UserAggregateService,
  ) {}
  async execute(command: AddNewUserCommand) {
    const { userPayload, retry, eventNumber } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addNewUser(userPayload, retry, eventNumber).toPromise();
    aggregate.commit();
  }
}
