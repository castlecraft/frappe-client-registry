import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveRoleCommand } from './remove-role.command';
import { RoleAggregateService } from '../../aggregates/role-aggregate/role-aggregate.service';

@CommandHandler(RemoveRoleCommand)
export class RemoveRoleHandler implements ICommandHandler<RemoveRoleCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: RoleAggregateService,
  ) {}
  async execute(command: RemoveRoleCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeRole(uuid).toPromise();
    aggregate.commit();
  }
}
