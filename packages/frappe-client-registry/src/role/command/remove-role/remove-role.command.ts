import { ICommand } from '@nestjs/cqrs';

export class RemoveRoleCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
