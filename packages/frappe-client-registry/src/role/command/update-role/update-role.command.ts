import { ICommand } from '@nestjs/cqrs';
import { UpdateRoleDto } from '../../../role/entity/role/update-role.dto';

export class UpdateRoleCommand implements ICommand {
  constructor(public readonly updatePayload: UpdateRoleDto) {}
}
