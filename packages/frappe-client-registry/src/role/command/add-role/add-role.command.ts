import { ICommand } from '@nestjs/cqrs';
import { RoleDto } from '../../entity/role/role-dto';

export class AddRoleCommand implements ICommand {
  constructor(
    public rolePayload: RoleDto,
    public readonly clientHttpRequest: any,
  ) {}
}
