import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AddRoleCommand } from './add-role.command';
import { RoleAggregateService } from '../../aggregates/role-aggregate/role-aggregate.service';

@CommandHandler(AddRoleCommand)
export class AddRoleHandler implements ICommandHandler<AddRoleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: RoleAggregateService,
  ) {}
  async execute(command: AddRoleCommand) {
    const { rolePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addRole(rolePayload, clientHttpRequest).toPromise();
    aggregate.commit();
  }
}
