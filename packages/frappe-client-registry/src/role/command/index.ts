import { AddRoleHandler } from './add-role/add-role.handler';
import { RemoveRoleHandler } from './remove-role/remove-role.handler';
import { UpdateRoleHandler } from './update-role/update-role.handler';

export const CommandManager = [
  AddRoleHandler,
  RemoveRoleHandler,
  UpdateRoleHandler,
];
