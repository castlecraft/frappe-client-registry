import { IsNotEmpty } from 'class-validator';

export class UpdateRoleDto {
  @IsNotEmpty()
  uuid: string;

  @IsNotEmpty()
  name: string;
}
