import { Module, HttpModule } from '@nestjs/common';
import { AggregatesManager } from './aggregates';
import { RoleEntitiesModule } from './entity/entity.module';
import { QueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { CommandManager } from './command';
import { EventManager } from './event';
import { RoleController } from './controllers/role/role.controller';
import { RolePoliciesService } from './policies/role-policies/role-policies.service';

@Module({
  imports: [RoleEntitiesModule, CqrsModule, HttpModule],
  controllers: [RoleController],
  providers: [
    ...AggregatesManager,
    ...QueryManager,
    ...EventManager,
    ...CommandManager,
    RolePoliciesService,
  ],
  exports: [RoleEntitiesModule],
})
export class RoleModule {}
