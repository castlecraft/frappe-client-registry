import { RoleUpdatedHandler } from './role-updated/role-updated.handler';
import { RoleRemovedHandler } from './role-removed/role.removed.handler';
import { RoleAddedHandler } from './role-added/role-added.handler';

export const EventManager = [
  RoleAddedHandler,
  RoleRemovedHandler,
  RoleUpdatedHandler,
];
