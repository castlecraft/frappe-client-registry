import { IEvent } from '@nestjs/cqrs';
import { Role } from '../../entity/role/role.entity';

export class RoleRemovedEvent implements IEvent {
  constructor(public role: Role) {}
}
