import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RoleService } from '../../entity/role/role.service';
import { RoleRemovedEvent } from './role-removed.event';

@EventsHandler(RoleRemovedEvent)
export class RoleRemovedHandler implements IEventHandler<RoleRemovedEvent> {
  constructor(private readonly remove: RoleService) {}
  async handle(event: RoleRemovedEvent) {
    const { role } = event;
    await this.remove.deleteOne({ uuid: role.uuid });
  }
}
