import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RoleService } from '../../entity/role/role.service';
import { RoleUpdatedEvent } from './role-updated.event';

@EventsHandler(RoleUpdatedEvent)
export class RoleUpdatedHandler implements IEventHandler<RoleUpdatedEvent> {
  constructor(private readonly object: RoleService) {}

  async handle(event: RoleUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: { name: updatePayload.name } },
    );
  }
}
