import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveRolesNotInCompanyQuery } from './retrieve-role-excluded-by-company.query';
import { RoleAggregateService } from '../../aggregates/role-aggregate/role-aggregate.service';

@QueryHandler(RetrieveRolesNotInCompanyQuery)
export class RetrieveRolesNotInCompanyHandler
  implements IQueryHandler<RetrieveRolesNotInCompanyQuery> {
  constructor(private readonly manager: RoleAggregateService) {}
  async execute(query: RetrieveRolesNotInCompanyQuery) {
    const { company, offset, limit, search, sort } = query;
    return await this.manager
      .getRoleExcludedByCompany(
        company,
        Number(offset),
        Number(limit),
        search,
        sort,
      )
      .toPromise();
  }
}
