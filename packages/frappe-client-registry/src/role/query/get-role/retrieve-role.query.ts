import { IQuery } from '@nestjs/cqrs';

export class RetrieveRoleQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
