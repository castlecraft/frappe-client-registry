import { IQuery } from '@nestjs/cqrs';

export class RetrieveRoleListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
  ) {}
}
