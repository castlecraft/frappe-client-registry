import {
  CanActivate,
  ExecutionContext,
  Injectable,
  BadRequestException,
} from '@nestjs/common';
import { USER_PROFILE_MISSING } from '../../constants/messages';

@Injectable()
export class ProfileGuard implements CanActivate {
  constructor() {}

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    if (!request.token.name || !request.token.email) {
      throw new BadRequestException(USER_PROFILE_MISSING);
    }
    return true;
  }
}
