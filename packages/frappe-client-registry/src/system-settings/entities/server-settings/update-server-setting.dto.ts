import { IsOptional } from 'class-validator';

export class UpdateServerSettingsDto {
  @IsOptional()
  clientUuid: string;

  @IsOptional()
  decafServerUrl: string;
}
