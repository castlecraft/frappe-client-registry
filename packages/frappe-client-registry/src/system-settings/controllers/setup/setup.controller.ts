import {
  Post,
  Controller,
  Body,
  UsePipes,
  ValidationPipe,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { SetupService } from './setup.service';
import { ServerSettingsDto } from '../../../system-settings/entities/server-settings/server-setting.dto';
import { RavenInterceptor } from 'nest-raven';

@Controller('setup')
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class SetupController {
  constructor(private readonly settingsService: SetupService) {}

  @Post()
  @UsePipes(ValidationPipe)
  async setup(@Body() payload: ServerSettingsDto) {
    return await this.settingsService.setup(payload);
  }
}
