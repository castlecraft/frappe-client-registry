import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { ClientRoleDto } from '../../entity/client-role/client-role-dto';
import { ClientRole } from '../../entity/client-role/client-role.entity';
import { ClientRoleRemovedEvent } from '../../event/client-role-removed/client-role-removed.event';
import { ClientRoleAddedEvent } from '../../event/client-role-added/client-role-added.event';
import { ClientRoleService } from '../../entity/client-role/client-role.service';
import { ClientRolePoliciesService } from '../../policies/client-role-policies/client-role-policies.service';
import { UserAssignedEvent } from '../../event/user-assigned/user-assigned.event';
import { from, of, throwError } from 'rxjs';
import { switchMap, mergeMap } from 'rxjs/operators';
import {
  CLIENT_NOT_FOUND,
  USER_ALREADY_EXIST,
  USER_NOT_FOUND,
} from '../../../constants/messages';
import { UserDeletedEvent } from '../../event/user-deleted/user-deleted.event';
import { ClientService } from '../../../client/entities/client/client.service';
import {
  ClientRoleResponseInterface,
  FrappeInstanceUserInfoMetaData,
} from '../../entity/client-role/client-role-response.interface';
import { UserInfoPoliciesService } from '../../policies/user-info-policies/user-info-policies.service';
import { AssignUserPoliciesService } from '../../policies/assign-user-policies/assign-user-policies.service';
import { Client } from '../../../client/entities/client/client.entity';

@Injectable()
export class ClientRoleAggregateService extends AggregateRoot {
  constructor(
    private readonly clientRoleService: ClientRoleService,
    private readonly clientRolePolicy: ClientRolePoliciesService,
    private readonly clientService: ClientService,
    private readonly userInfoPolicy: UserInfoPoliciesService,
    private readonly assignUserPolicy: AssignUserPoliciesService,
  ) {
    super();
  }

  addClientRole(clientRolePayload: ClientRoleDto, clientHttpRequest) {
    return from(
      this.assignUserPolicy.validateUsersInClient(clientRolePayload),
    ).pipe(
      switchMap(existingUser => {
        if (existingUser.length !== 0) {
          return throwError(new BadRequestException(USER_ALREADY_EXIST));
        }
        return from(
          this.clientRolePolicy.validateClientRole(
            clientRolePayload,
            clientHttpRequest,
          ),
        ).pipe(
          switchMap(client => {
            const clientRole = Object.assign(
              new ClientRole(),
              clientRolePayload,
            );
            clientRole.uuid = uuidv4();
            this.apply(new ClientRoleAddedEvent(clientRole, client));
            return of(client);
          }),
        );
      }),
    );
  }

  async retrieveClientRole(uuid: string, req) {
    const provider = await this.clientRoleService.findOne({ uuid });
    if (!provider) {
      throw new NotFoundException();
    }
    return provider;
  }

  retrieveUserInfo(clientHttpRequest) {
    return this.userInfoPolicy.validateInfoUser(clientHttpRequest).pipe(
      switchMap(user => {
        return of(
          this.clientRoleService.aggregate([
            { $match: { users: { $in: [clientHttpRequest.token.email] } } },
            {
              $group: {
                _id: '$frappeClientUuid',
                roles: { $push: '$name' },
                clientUuid: { $addToSet: '$clientUuid' },
              },
            },
            { $unwind: '$clientUuid' },
            {
              $project: {
                frappeClientUuid: '$_id',
                _id: false,
                roles: true,
                clientUuid: true,
              },
            },
          ]),
        );
      }),
      mergeMap((clientRoles: any) => {
        const userInfoMetaData: ClientRoleResponseInterface = {
          name: clientHttpRequest.token.name,
          email: clientHttpRequest.token.email,
          instances: [],
        };
        return from(clientRoles.toArray()).pipe(
          switchMap((aggregateData: FrappeInstanceUserInfoMetaData[]) => {
            if (!aggregateData || aggregateData.length === 0) {
              return of(userInfoMetaData);
            }
            return from(aggregateData).pipe(
              mergeMap((instance: FrappeInstanceUserInfoMetaData) => {
                if (!instance) {
                  return of(userInfoMetaData);
                }
                return this.frappeUserInfoResponse(userInfoMetaData, instance);
              }),
            );
          }),
        );
      }),
    );
  }

  frappeUserInfoResponse(
    userInfoMetaData: ClientRoleResponseInterface,
    frappeInstance: FrappeInstanceUserInfoMetaData,
  ) {
    return from(
      this.clientService.aggregate([
        { $match: { uuid: frappeInstance.clientUuid } },
        {
          $project: {
            frappeInstance: {
              $filter: {
                input: '$frappeInstance',
                as: 'frappeInstance',
                cond: {
                  $eq: [
                    '$$frappeInstance.frappeClientUuid',
                    frappeInstance.frappeClientUuid,
                  ],
                },
              },
            },
            name: 1,
          },
        },
        { $unwind: '$frappeInstance' },
      ]),
    ).pipe(
      switchMap((aggregateData: any) => {
        return from(aggregateData.toArray()).pipe(
          switchMap((client: any) => {
            frappeInstance.clientName = client[0].name;
            frappeInstance.instanceName = client[0].frappeInstance.name;
            userInfoMetaData.instances.push(frappeInstance);
            return of(userInfoMetaData);
          }),
        );
      }),
    );
  }

  async getClientRoleList(offset, limit, search, sort) {
    return this.clientRoleService.list(offset, limit, search, sort);
  }

  async removeClientRole(uuid: string) {
    const provider = await this.clientRoleService.findOne({ uuid });
    if (!provider) {
      throw new NotFoundException();
    }
    this.apply(new ClientRoleRemovedEvent(provider));
  }

  assignNewUser(clientRolePayload, clientHttpRequest) {
    return from(this.assignUserPolicy.validateUser(clientRolePayload)).pipe(
      switchMap(assignUser => {
        return this.clientRolePolicy
          .validateExistingUser(clientRolePayload)
          .pipe(
            switchMap(existingUser => {
              if (existingUser) {
                return throwError(new BadRequestException(USER_ALREADY_EXIST));
              }
              return this.clientRolePolicy
                .validateUsers(clientRolePayload)
                .pipe(
                  switchMap(isValid => {
                    return from(
                      this.clientRoleService.findOne({
                        uuid: clientRolePayload.uuid,
                        frappeClientUuid: clientRolePayload.frappeClientUuid,
                      }),
                    ).pipe(
                      switchMap(clientRole => {
                        if (!clientRole) {
                          return throwError(
                            new NotFoundException(CLIENT_NOT_FOUND),
                          );
                        }
                        clientRolePayload.clientUuid = clientRole.clientUuid;
                        clientRolePayload.name = clientRole.name;
                        clientRolePayload.frappeClientUuid =
                          clientRole.frappeClientUuid;

                        return this.getFrappeInstanceAggregate(
                          clientRolePayload,
                        ).pipe(
                          switchMap((aggregateData: Client[]) => {
                            this.apply(
                              new UserAssignedEvent(
                                clientRolePayload,
                                aggregateData[0],
                              ),
                            );
                            return of();
                          }),
                        );
                      }),
                    );
                  }),
                );
            }),
          );
      }),
    );
  }

  deleteUser(clientRolePayload, clientHttpRequest) {
    return this.clientRolePolicy.validateExistingUser(clientRolePayload).pipe(
      switchMap(clientRole => {
        if (!clientRole) {
          return throwError(new BadRequestException(USER_NOT_FOUND));
        }
        clientRolePayload.clientUuid = clientRole.clientUuid;
        clientRolePayload.name = clientRole.name;
        return this.getFrappeInstanceAggregate(clientRolePayload).pipe(
          switchMap((aggregateData: Client[]) => {
            this.apply(
              new UserDeletedEvent(clientRolePayload, aggregateData[0]),
            );
            return of({});
          }),
        );
      }),
    );
  }

  getFrappeInstanceAggregate(clientRolePayload) {
    return this.clientService.asyncAggregate([
      { $match: { uuid: clientRolePayload.clientUuid } },
      {
        $project: {
          frappeInstance: {
            $filter: {
              input: '$frappeInstance',
              as: 'frappeInstance',
              cond: {
                $eq: [
                  '$$frappeInstance.frappeClientUuid',
                  clientRolePayload.frappeClientUuid,
                ],
              },
            },
          },
          name: 1,
          uuid: 1,
        },
      },
      { $unwind: '$frappeInstance' },
      {
        $project: {
          _id: 0,
          frappeInstance: {
            bloomstackBotPassword: 0,
          },
        },
      },
    ]);
  }
}
