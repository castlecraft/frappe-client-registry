import { IQuery } from '@nestjs/cqrs';

export class RetrieveUserInfoQuery implements IQuery {
  constructor(public readonly clientHttpReq: any) {}
}
