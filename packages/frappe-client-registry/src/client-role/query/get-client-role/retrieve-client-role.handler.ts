import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveClientRoleQuery } from './retrieve-client-role.query';
import { ClientRoleAggregateService } from '../../aggregates/client-role-aggregate/client-role-aggregate.service';

@QueryHandler(RetrieveClientRoleQuery)
export class RetrieveClientRoleHandler
  implements IQueryHandler<RetrieveClientRoleQuery> {
  constructor(private readonly manager: ClientRoleAggregateService) {}

  async execute(query: RetrieveClientRoleQuery) {
    const { req, uuid } = query;
    return this.manager.retrieveClientRole(uuid, req);
  }
}
