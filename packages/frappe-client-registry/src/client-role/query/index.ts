import { RetrieveClientRoleHandler } from './get-client-role/retrieve-client-role.handler';
import { RetrieveClientRoleListHandler } from './list-client-role/retrieve-client-role-list.handler';
import { RetrieveUserInfoHandler } from './get-user-info/retrieve-user-info.handler';

export const QueryManager = [
  RetrieveClientRoleHandler,
  RetrieveClientRoleListHandler,
  RetrieveUserInfoHandler,
];
