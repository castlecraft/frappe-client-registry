import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveClientRoleListQuery } from './retrieve-client-role-list.query';
import { ClientRoleAggregateService } from '../../aggregates/client-role-aggregate/client-role-aggregate.service';

@QueryHandler(RetrieveClientRoleListQuery)
export class RetrieveClientRoleListHandler
  implements IQueryHandler<RetrieveClientRoleListQuery> {
  constructor(private readonly manager: ClientRoleAggregateService) {}
  async execute(query: RetrieveClientRoleListQuery) {
    const { offset, limit, search, sort } = query;
    return await this.manager.getClientRoleList(
      Number(offset),
      Number(limit),
      search,
      sort,
    );
  }
}
