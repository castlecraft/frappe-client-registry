import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserDeletedEvent } from './user-deleted.event';
import { ClientRoleService } from '../../entity/client-role/client-role.service';

@EventsHandler(UserDeletedEvent)
export class UserDeletedHandler implements IEventHandler<UserDeletedEvent> {
  constructor(private readonly clientRoleService: ClientRoleService) {}

  async handle(event: UserDeletedEvent) {
    const { clientRolePayload } = event;
    await this.clientRoleService.updateOne(
      { uuid: clientRolePayload.uuid },
      { $pull: { users: { $in: clientRolePayload.users } } },
    );
  }
}
