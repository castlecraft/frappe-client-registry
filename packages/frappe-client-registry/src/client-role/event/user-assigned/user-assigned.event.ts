import { IEvent } from '@nestjs/cqrs';
import { ClientRole } from '../../entity/client-role/client-role.entity';
import { Client } from '../../../client/entities/client/client.entity';

export class UserAssignedEvent implements IEvent {
  constructor(public clientRolePayload: ClientRole, public client: Client) {}
}
