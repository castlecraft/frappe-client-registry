import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { UserAssignedEvent } from './user-assigned.event';
import { ClientRoleService } from '../../entity/client-role/client-role.service';

@EventsHandler(UserAssignedEvent)
export class UserAssignedHandler implements IEventHandler<UserAssignedEvent> {
  constructor(private readonly clientRoleService: ClientRoleService) {}

  async handle(event: UserAssignedEvent) {
    const { clientRolePayload } = event;
    await this.clientRoleService.updateOne(
      { uuid: clientRolePayload.uuid },
      { $push: { users: { $each: clientRolePayload.users } } },
    );
  }
}
