import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientRoleAddedEvent } from './client-role-added.event';
import { ClientRoleService } from '../../entity/client-role/client-role.service';

@EventsHandler(ClientRoleAddedEvent)
export class ClientRoleAddedHandler
  implements IEventHandler<ClientRoleAddedEvent> {
  constructor(private readonly clientRoleService: ClientRoleService) {}
  async handle(event: ClientRoleAddedEvent) {
    const { clientRolePayload } = event;
    await this.clientRoleService.create(clientRolePayload);
  }
}
