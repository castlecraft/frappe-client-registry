import { IEvent } from '@nestjs/cqrs';
import { ClientRole } from '../../entity/client-role/client-role.entity';

export class ClientRoleRemovedEvent implements IEvent {
  constructor(public found: ClientRole) {}
}
