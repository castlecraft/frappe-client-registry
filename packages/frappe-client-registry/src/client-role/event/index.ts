import { ClientRoleAddedHandler } from './client-role-added/client-role-added.handler';
import { ClientRoleRemovedHandler } from './client-role-removed/client-role.removed.handler';
import { UserAssignedHandler } from './user-assigned/user-assigned.handler';
import { UserDeletedHandler } from './user-deleted/user-deleted.handler';

export const EventManager = [
  ClientRoleAddedHandler,
  ClientRoleRemovedHandler,
  UserAssignedHandler,
  UserDeletedHandler,
];
