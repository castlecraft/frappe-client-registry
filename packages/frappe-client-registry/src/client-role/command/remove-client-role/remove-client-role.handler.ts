import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveClientRoleCommand } from './remove-client-role.command';
import { ClientRoleAggregateService } from '../../aggregates/client-role-aggregate/client-role-aggregate.service';

@CommandHandler(RemoveClientRoleCommand)
export class RemoveClientRoleHandler
  implements ICommandHandler<RemoveClientRoleCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: ClientRoleAggregateService,
  ) {}
  async execute(command: RemoveClientRoleCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeClientRole(uuid);
    aggregate.commit();
  }
}
