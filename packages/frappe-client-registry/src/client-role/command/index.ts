import { AddClientRoleHandler } from './add-client-role/add-client-role.handler';
import { RemoveClientRoleHandler } from './remove-client-role/remove-client-role.handler';
import { AssignNewUserHandler } from './assign-new-user/assign-new-user.handler';
import { DeleteUserHandler } from './delete-user/delete-user.handler';

export const CommandManager = [
  AddClientRoleHandler,
  RemoveClientRoleHandler,
  AssignNewUserHandler,
  DeleteUserHandler,
];
