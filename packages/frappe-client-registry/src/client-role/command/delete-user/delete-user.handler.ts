import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { DeleteUserCommand } from './delete-user.command';
import { ClientRoleAggregateService } from '../../aggregates/client-role-aggregate/client-role-aggregate.service';

@CommandHandler(DeleteUserCommand)
export class DeleteUserHandler implements ICommandHandler<DeleteUserCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientRoleAggregateService,
  ) {}
  async execute(command: DeleteUserCommand) {
    const { userPayload: clientRolePayload, clientHttpRequest } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate
      .deleteUser(clientRolePayload, clientHttpRequest)
      .toPromise();
    aggregate.commit();
  }
}
