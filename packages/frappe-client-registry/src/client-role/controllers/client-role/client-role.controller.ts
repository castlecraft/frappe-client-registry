import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ClientRoleDto } from '../../entity/client-role/client-role-dto';
import { AddClientRoleCommand } from '../../command/add-client-role/add-client-role.command';
import { RemoveClientRoleCommand } from '../../command/remove-client-role/remove-client-role.command';
import { RetrieveClientRoleQuery } from '../../query/get-client-role/retrieve-client-role.query';
import { RetrieveClientRoleListQuery } from '../../query/list-client-role/retrieve-client-role-list.query';
import { AssignNewUserCommand } from '../../command/assign-new-user/assign-new-user.command';
import { AssignUserDto } from '../../entity/client-role/assign-user-dto';
import { DeleteUserCommand } from '../../command/delete-user/delete-user.command';
import { RetrieveUserInfoQuery } from '../../query/get-user-info/retrieve-user-info.query';
import { ProfileGuard } from '../../../auth/guards/profile.guard';
import { RavenInterceptor } from 'nest-raven';

@Controller('client-role')
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class ClientRoleController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() clientRolePayload: ClientRoleDto, @Req() req) {
    return this.commandBus.execute(
      new AddClientRoleCommand(clientRolePayload, req),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  get(@Param('uuid') uuid, @Req() req) {
    return this.queryBus.execute(new RetrieveClientRoleQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  listRole(
    @Query('start') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveClientRoleListQuery(offset, limit, search, sort),
    );
  }

  @Get('v1/user_info')
  @UseGuards(TokenGuard, ProfileGuard)
  userInfo(@Req() clientHttpReq) {
    return this.queryBus.execute(new RetrieveUserInfoQuery(clientHttpReq));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveClientRoleCommand(uuid));
  }

  @Post('v1/add_user')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  assignUser(
    @Body() clientRolePayload: AssignUserDto,
    @Req() clientHttpRequest,
  ) {
    return this.commandBus.execute(
      new AssignNewUserCommand(clientRolePayload, clientHttpRequest),
    );
  }

  @Post('v1/delete_user')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  deleteUser(
    @Body() clientRolePayload: AssignUserDto,
    @Req() clientHttpRequest,
  ) {
    return this.commandBus.execute(
      new DeleteUserCommand(clientRolePayload, clientHttpRequest),
    );
  }
}
