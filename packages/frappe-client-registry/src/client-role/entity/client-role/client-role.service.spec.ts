import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ClientRoleService } from './client-role.service';
import { ClientRole } from './client-role.entity';

describe('ClientRoleService', () => {
  let service: ClientRoleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClientRoleService,
        {
          provide: getRepositoryToken(ClientRole),
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ClientRoleService>(ClientRoleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
