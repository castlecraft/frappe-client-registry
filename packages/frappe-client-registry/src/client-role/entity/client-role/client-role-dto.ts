import {
  IsNotEmpty,
  IsArray,
  ArrayMaxSize,
  ArrayMinSize,
} from 'class-validator';

export class ClientRoleDto {
  @IsNotEmpty()
  name: string;

  @IsArray()
  @ArrayMaxSize(1)
  @ArrayMinSize(1)
  users: string[];

  @IsNotEmpty()
  clientUuid: string;

  @IsNotEmpty()
  frappeClientUuid: string;
}
