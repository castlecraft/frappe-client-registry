import { Test, TestingModule } from '@nestjs/testing';
import { UserInfoPoliciesService } from './user-info-policies.service';
import { UserService } from '../../../user/entities/user/user.service';
import { UserAggregateService } from '../../../user/aggregates/user-aggregate/user-aggregate.service';

describe('UserInfoPoliciesService', () => {
  let service: UserInfoPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserInfoPoliciesService,
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: UserAggregateService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<UserInfoPoliciesService>(UserInfoPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
