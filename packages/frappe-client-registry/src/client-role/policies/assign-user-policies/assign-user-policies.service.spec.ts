import { Test, TestingModule } from '@nestjs/testing';
import { AssignUserPoliciesService } from './assign-user-policies.service';
import { ClientRoleService } from '../../entity/client-role/client-role.service';

describe('AssignUserPoliciesService', () => {
  let service: AssignUserPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AssignUserPoliciesService,
        {
          provide: ClientRoleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AssignUserPoliciesService>(AssignUserPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
