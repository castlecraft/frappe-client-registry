import { Injectable, BadRequestException } from '@nestjs/common';
import { ClientRoleService } from '../../entity/client-role/client-role.service';
import {
  CLIENT_ROLE_ALREADY_EXIST,
  CLIENT_NOT_FOUND,
  INVALID_USER_IN_ARRAY,
  INVALID_ROLE,
  FRAPPE_INSTANCE_DOES_NOT_EXIST,
} from '../../../constants/messages';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { UserService } from '../../../user/entities/user/user.service';
import { ClientService } from '../../../client/entities/client/client.service';
import { RoleService } from '../../../role/entity/role/role.service';
import { ClientRoleDto } from '../../entity/client-role/client-role-dto';

@Injectable()
export class ClientRolePoliciesService {
  constructor(
    private readonly clientRoleService: ClientRoleService,
    private readonly userService: UserService,
    private readonly clientService: ClientService,
    private readonly roleService: RoleService,
  ) {}

  validateClientRole(clientRolePayload: ClientRoleDto, clientHttpRequest) {
    return from(
      this.clientRoleService.findOne(
        {
          clientUuid: clientRolePayload.clientUuid,
          name: clientRolePayload.name,
          frappeClientUuid: clientRolePayload.frappeClientUuid,
        },
        {
          users: { $slice: 5 },
        },
      ),
    ).pipe(
      switchMap(clientRole => {
        if (clientRole) {
          return throwError(new BadRequestException(CLIENT_ROLE_ALREADY_EXIST));
        }
        return this.validateUsers(clientRolePayload).pipe(
          switchMap(isValid => {
            return this.validateClient(clientRolePayload).pipe(
              switchMap(client => {
                return this.validateRole(clientRolePayload, client);
              }),
            );
          }),
        );
      }),
    );
  }

  validateUsers(clientRolePayload: ClientRoleDto) {
    return from(
      this.userService.count({
        email: { $in: clientRolePayload.users },
      }),
    ).pipe(
      switchMap(userCount => {
        if (userCount !== clientRolePayload.users.length) {
          return throwError(new BadRequestException(INVALID_USER_IN_ARRAY));
        }
        return of(true);
      }),
    );
  }

  validateClient(clientRolePayload: ClientRoleDto) {
    return from(
      this.clientService.asyncAggregate([
        { $match: { uuid: clientRolePayload.clientUuid } },
        {
          $project: {
            frappeInstance: {
              $filter: {
                input: '$frappeInstance',
                as: 'frappeInstance',
                cond: {
                  $eq: [
                    '$$frappeInstance.frappeClientUuid',
                    clientRolePayload.frappeClientUuid,
                  ],
                },
              },
            },
            name: 1,
            uuid: 1,
          },
        },
        { $unwind: '$frappeInstance' },
        {
          $project: {
            _id: 0,
            frappeInstance: {
              bloomstackBotPassword: 0,
            },
          },
        },
      ]),
    ).pipe(
      switchMap((client: any[]) => {
        if (!client) {
          return throwError(new BadRequestException(CLIENT_NOT_FOUND));
        }
        return from(this.validateFrappeInstance(clientRolePayload)).pipe(
          switchMap(frappeInstance => {
            return of(client.length > 1 ? client : client[0]);
          }),
        );
      }),
    );
  }

  validateRole(clientRolePayload, client?) {
    return from(
      this.roleService.findOne({ name: clientRolePayload.name }),
    ).pipe(
      switchMap(role => {
        if (!role) return throwError(new BadRequestException(INVALID_ROLE));
        return of(client);
      }),
    );
  }

  validateExistingUser(clientRolePayload) {
    return from(
      this.clientRoleService.findOne({
        uuid: clientRolePayload.uuid,
        users: { $in: clientRolePayload.users },
      }),
    );
  }

  validateFrappeInstance(clientRolePayload: ClientRoleDto) {
    return from(
      this.clientService.aggregate([
        { $match: { uuid: clientRolePayload.clientUuid } },
        {
          $project: {
            frappeInstance: {
              $filter: {
                input: '$frappeInstance',
                as: 'singleInstance',
                cond: {
                  $eq: [
                    '$$singleInstance.frappeClientUuid',
                    clientRolePayload.frappeClientUuid,
                  ],
                },
              },
            },
          },
        },
      ]),
    ).pipe(
      switchMap((instance: any) => {
        return from(instance.toArray()).pipe(
          switchMap((aggregateData: any) => {
            if (
              aggregateData.length === 0 ||
              !aggregateData[0].frappeInstance ||
              aggregateData[0].frappeInstance === null
            ) {
              return throwError(
                new BadRequestException(FRAPPE_INSTANCE_DOES_NOT_EXIST),
              );
            }
            return of(true);
          }),
        );
      }),
    );
  }
}
