import { Test, TestingModule } from '@nestjs/testing';
import { ClientRolePoliciesService } from './client-role-policies.service';
import { ClientRoleService } from '../../entity/client-role/client-role.service';
import { UserService } from '../../../user/entities/user/user.service';
import { ClientService } from '../../../client/entities/client/client.service';
import { RoleService } from '../../../role/entity/role/role.service';

describe('ClientRolePoliciesService', () => {
  let service: ClientRolePoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClientRolePoliciesService,
        {
          provide: ClientRoleService,
          useValue: {},
        },
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: ClientService,
          useValue: {},
        },
        {
          provide: RoleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ClientRolePoliciesService>(ClientRolePoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
