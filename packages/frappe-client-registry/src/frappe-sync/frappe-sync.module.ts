import { frappeSyncAggregatesManager } from './aggregates';
import { Global, Module, HttpModule } from '@nestjs/common';

@Global()
@Module({
  imports: [HttpModule],
  controllers: [],
  providers: [
    // CQRS
    ...frappeSyncAggregatesManager,
  ],
  exports: [...frappeSyncAggregatesManager],
})
export class FrappeSyncModule {}
