import { Test, TestingModule } from '@nestjs/testing';
import { RecursiveSyncService } from './recursive-sync.service';
import { HttpService } from '@nestjs/common';

describe('RecursiveSyncService', () => {
  let service: RecursiveSyncService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RecursiveSyncService,
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<RecursiveSyncService>(RecursiveSyncService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
