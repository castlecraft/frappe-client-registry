import { Injectable, HttpService, NotFoundException } from '@nestjs/common';
import { BEARER } from '../../../constants/app-strings';
import { map, switchMap } from 'rxjs/operators';
import { of, timer, throwError } from 'rxjs';
import { HttpMethod } from '../../../constants/request-methods';
import { FRAPPE_CONNECTOR_LOG_ENDPOINT } from '../../../constants/url-endpoints';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { OutgoingHttpHeaders } from 'http';

@Injectable()
export class RecursiveSyncService {
  constructor(private readonly http: HttpService) {}

  retryAndFetchResponse(
    token: TokenCache,
    frappeConnectorRequestLogURL: string,
    url: string,
    requestMethod: HttpMethod,
    successCallback?: CallableFunction,
    failureCallback?: CallableFunction,
    callbackParams?: any,
    frappeObject?,
  ) {
    return this.http
      .get(frappeConnectorRequestLogURL, {
        headers: {
          Authorization: BEARER + token.accessToken,
        },
      })
      .pipe(map(res => res.data))
      .pipe(
        switchMap(success => {
          if (success.successResponse) {
            successCallback &&
              successCallback(success.successResponse, callbackParams);
            return of({});
          }

          if (success.failResponse) {
            failureCallback &&
              failureCallback(url, requestMethod, frappeObject, callbackParams);
            return of({});
          }

          return timer(2000).pipe(
            switchMap((): any => {
              this.retryAndFetchResponse(
                token,
                frappeConnectorRequestLogURL,
                url,
                requestMethod,
                successCallback,
                failureCallback,
                callbackParams,
                frappeObject,
              );
              return of({});
            }),
          );
        }),
      )
      .subscribe({
        next: success => {},
        error: error => {},
      });
  }

  pushAndSyncToFrappe(
    url: string,
    headers: OutgoingHttpHeaders,
    token: TokenCache,
    requestMethod: HttpMethod,
    supportPortalInfo: { frappeConnectorURL: string; frappeServerUuid: string },
    successCallback?: CallableFunction,
    failureCallback?: CallableFunction,
    callbackParams?: any,
    frappeObject?,
  ) {
    const process = switchMap((response: { data: { requestLog: any } }) => {
      if (response.data) {
        const frappeConnectorRequestLogURL =
          supportPortalInfo.frappeConnectorURL +
          FRAPPE_CONNECTOR_LOG_ENDPOINT +
          response.data.requestLog;

        this.retryAndFetchResponse(
          token,
          frappeConnectorRequestLogURL,
          url,
          requestMethod,
          successCallback,
          failureCallback,
          callbackParams,
          frappeObject,
        );
        return of({});
      } else {
        return throwError(new NotFoundException());
      }
    });

    const subscription = {
      next: success => {},
      error: error => {},
    };

    switch (requestMethod) {
      case HttpMethod.POST:
        this.http
          .post(url, frappeObject, { headers })
          .pipe(process)
          .subscribe(subscription);
        break;
      case HttpMethod.PUT:
        this.http
          .put(url, frappeObject, { headers })
          .pipe(process)
          .subscribe(subscription);
        break;
      case HttpMethod.GET:
        this.http
          .get(url, { headers })
          .pipe(process)
          .subscribe(subscription);
        break;
      default:
        break;
    }
  }
}
