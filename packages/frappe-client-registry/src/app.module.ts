import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { typeOrmFactory } from './constants/typeorm.connection';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';
import { ClientModule } from './client/client.module';
import { UserModule } from './user/user.module';
import { RoleModule } from './role/role.module';
import { ClientRoleModule } from './client-role/client-role.module';
import { ConfigService } from './config/config.service';
import { TerminusModule } from '@nestjs/terminus';
import { TerminusOptionsService } from './system-settings/aggregates/terminus-options/terminus-options.service';
import { EventStoreModule } from './event-store/event-store.module';
import { FrappeSyncModule } from './frappe-sync/frappe-sync.module';
import { RavenModule } from 'nest-raven';

@Module({
  imports: [
    RavenModule,
    HttpModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: typeOrmFactory,
    }),
    TerminusModule.forRootAsync({ useClass: TerminusOptionsService }),
    ConfigModule,
    AuthModule,
    SystemSettingsModule,
    ClientModule,
    UserModule,
    RoleModule,
    ClientRoleModule,
    EventStoreModule,
    FrappeSyncModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
