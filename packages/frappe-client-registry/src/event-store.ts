import { INestApplication, Logger } from '@nestjs/common';
import { EventStoreServer } from './event-store/microservice/event-store.server';
import { ConfigService, AS_ES_STREAM } from './config/config.service';

export const ES_STARTED_SUCCESSFULLY = 'Event Store started successfully';
export const AS_ES_STARTED_SUCCESSFULLY =
  'Authorization Server Event Store started successfully';

export function setupEventStore(app: INestApplication) {
  const config = app.get<ConfigService>(ConfigService);
  const logger = app.get<Logger>(Logger);

  const { hostname, username, password, stream } = config.getEventStoreConfig();
  if (hostname && username && password && stream) {
    const eventStore = app.connectMicroservice({
      strategy: new EventStoreServer(hostname, username, password, stream),
    });
    eventStore.listen(() =>
      logger.log(ES_STARTED_SUCCESSFULLY, eventStore.constructor.name),
    );
  }

  const authServerStream = config.get(AS_ES_STREAM);

  if (hostname && username && password && authServerStream) {
    const authServerEventStore = app.connectMicroservice({
      strategy: new EventStoreServer(
        hostname,
        username,
        password,
        authServerStream,
      ),
    });
    authServerEventStore.listen(() =>
      logger.log(
        AS_ES_STARTED_SUCCESSFULLY,
        authServerEventStore.constructor.name,
      ),
    );
  }
}
