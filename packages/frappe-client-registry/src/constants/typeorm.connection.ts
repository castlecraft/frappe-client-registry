import { MongoConnectionOptions } from 'typeorm/driver/mongodb/MongoConnectionOptions';
import {
  ConfigService,
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_NAME,
} from '../config/config.service';
import { ServerSettings } from '../system-settings/entities/server-settings/server-settings.entity';
import { TokenCache } from '../auth/entities/token-cache/token-cache.entity';
import { Client } from '../client/entities/client/client.entity';
import { User } from '../user/entities/user/user.entity';
import { Role } from '../role/entity/role/role.entity';
import { ClientRole } from '../client-role/entity/client-role/client-role.entity';

export function typeOrmFactory(config: ConfigService): MongoConnectionOptions {
  return {
    type: 'mongodb',
    url: `mongodb://${config.get(DB_USER)}:${config.get(
      DB_PASSWORD,
    )}@${config.get(DB_HOST)}/${config.get(DB_NAME)}?useUnifiedTopology=true`,
    logging: false,
    synchronize: true,
    entities: [ServerSettings, TokenCache, Client, User, Role, ClientRole],
    useNewUrlParser: true,
  };
}
