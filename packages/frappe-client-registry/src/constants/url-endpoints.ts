export const INFO_ENDPOINT = '/info';
export const SERVICE_TYPE_CREATE_ENDPOINT = '/service_type/v1/create';
export const SERVICE_REGISTER_ENDPOINT = '/service/v1/register';
export const GLOBAL_API_PREFIX = '/api';
export const SERVICE_RETRIEVE_ENDPOINT = '/service/v1/list?type=';
export const FRAPPE_CONNECTOR_COMMAND_ENDPOINT = '/frappe/v1/command/';
export const FRAPPE_CONNECTOR_TRUSTED_CLIENT_ENDPOINT =
  '/frappe/v1/command_as_trusted_client/';
export const ERP_GET_ROLE_ENDPOINT =
  '/api/method/frappe.core.doctype.user.user.get_roles';
export const ERP_SET_ROLE_ENDPOINT =
  '/api/method/bloomstack_core.services.user.set_roles';
export const FRAPPE_CONNECTOR_LOG_ENDPOINT = '/request_log/v1/get/';
export const GET_SERVICE_TYPE_ENDPOINT = '/service/v1/list?type=';
export const DECAF_SERVER_GET_CLIENT_ENDPOINT = '/frappe/v1/get/';
