export interface EventStoreInterface {
  streamId: string;
  eventId: string;
  eventNumber: number;
  eventType: string;
  created: string;
  metadata: UserInterface;
  isJson: boolean;
  data: any;
}

export class UserInterface {
  email: string;
  name: string;
  socialLogin: string;
}
