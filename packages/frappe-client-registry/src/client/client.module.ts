import { Module, HttpModule } from '@nestjs/common';
import { ClientController } from './controllers/client/client.controller';
import { ClientEntitiesModule } from './entities/entity.module';
import { AggregateManager } from './aggregates';
import { EventManager } from './events';
import { CommandManager } from './commands';
import { CqrsModule } from '@nestjs/cqrs';
import { QueryManager } from './query';
import { ClientPoliciesService } from './policies/client-policies/client-policies.service';
import { FrappeInstancePoliciesService } from './policies/frappe-instance-policies/frappe-instance-policies.service';

@Module({
  controllers: [ClientController],
  imports: [ClientEntitiesModule, CqrsModule, HttpModule],
  providers: [
    ...AggregateManager,
    ...EventManager,
    ...CommandManager,
    ...QueryManager,
    ClientPoliciesService,
    FrappeInstancePoliciesService,
  ],
  exports: [ClientEntitiesModule],
})
export class ClientModule {}
