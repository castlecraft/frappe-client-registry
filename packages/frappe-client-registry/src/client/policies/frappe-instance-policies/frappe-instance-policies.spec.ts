import { Test, TestingModule } from '@nestjs/testing';
import { FrappeInstancePoliciesService } from './frappe-instance-policies.service';
import { HttpService } from '@nestjs/common';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientService } from '../../entities/client/client.service';

describe('FrappeInstancePoliciesService', () => {
  let service: FrappeInstancePoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FrappeInstancePoliciesService,
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<FrappeInstancePoliciesService>(
      FrappeInstancePoliciesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
