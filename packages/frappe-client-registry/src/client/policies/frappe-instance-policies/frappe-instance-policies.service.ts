import { Injectable, HttpService, BadRequestException } from '@nestjs/common';
import { AddNewFrappeInstanceDto } from '../../entities/client/add-frappe-instance.dto';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { switchMap, catchError } from 'rxjs/operators';
import { ServerSettings } from '../../../system-settings/entities/server-settings/server-settings.entity';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { DECAF_SERVER_GET_CLIENT_ENDPOINT } from '../../../constants/url-endpoints';
import { throwError, of, from } from 'rxjs';
import {
  FRAPPE_CLIENT_NOT_FOUND,
  FRAPPE_INSTANCE_NAME_ALREADY_EXIST,
  FRAPPE_INSTANCE_ALREADY_EXIST,
} from '../../../constants/messages';
import { BEARER } from '../../../constants/app-strings';
import { TokenCache } from '../../../auth/entities/token-cache/token-cache.entity';
import { ClientService } from '../../entities/client/client.service';
import { FrappeInstanceMetadata } from '../../entities/client/client.entity';

@Injectable()
export class FrappeInstancePoliciesService {
  constructor(
    private readonly settingService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenService: ClientTokenManagerService,
    private readonly clientService: ClientService,
  ) {}

  validateFrappeInstance(clientPayload: AddNewFrappeInstanceDto) {
    return from(
      this.clientService.find({
        'frappeInstance.frappeClientUuid': clientPayload.frappeClientUuid,
      }),
    ).pipe(
      switchMap(client => {
        if (client.length && client.length > 0) {
          return throwError(
            new BadRequestException(FRAPPE_INSTANCE_ALREADY_EXIST),
          );
        }
        return this.settingService.find().pipe(
          switchMap((settings: ServerSettings) => {
            return this.clientTokenService.getClientToken().pipe(
              switchMap((token: TokenCache) => {
                return this.http
                  .get(
                    settings.decafServerUrl +
                      DECAF_SERVER_GET_CLIENT_ENDPOINT +
                      clientPayload.frappeClientUuid,
                    {
                      headers: {
                        authorization: `${BEARER} ${token.accessToken}`,
                      },
                    },
                  )
                  .pipe(
                    switchMap(decafClient => {
                      return this.validateFrappeInstanceName(clientPayload);
                    }),
                    catchError(error => {
                      return throwError(
                        new BadRequestException(
                          error.getResponse
                            ? error.getResponse().message
                            : FRAPPE_CLIENT_NOT_FOUND,
                        ),
                      );
                    }),
                  );
              }),
            );
          }),
        );
      }),
    );
  }

  validateFrappeInstanceName(clientPayload: AddNewFrappeInstanceDto) {
    const nameQuery = { $eq: ['$$singleInstance.name', clientPayload.name] };
    const frappeClientQuery = {
      $eq: [
        '$$singleInstance.frappeClientUuid',
        clientPayload.frappeClientUuid,
      ],
    };
    return from(
      this.clientService.aggregate([
        { $match: { uuid: clientPayload.clientUuid } },
        {
          $project: {
            frappeInstance: {
              $filter: {
                input: '$frappeInstance',
                as: 'singleInstance',
                cond: { $or: [nameQuery, frappeClientQuery] },
              },
            },
          },
        },
      ]),
    ).pipe(
      switchMap((instance: any) => {
        return from(instance.toArray()).pipe(
          switchMap(
            (aggregateData: { frappeInstance: FrappeInstanceMetadata[] }) => {
              if (
                aggregateData[0].frappeInstance &&
                aggregateData[0].frappeInstance.length > 0
              ) {
                return throwError(
                  new BadRequestException(FRAPPE_INSTANCE_NAME_ALREADY_EXIST),
                );
              }
              return of(true);
            },
          ),
        );
      }),
    );
  }
}
