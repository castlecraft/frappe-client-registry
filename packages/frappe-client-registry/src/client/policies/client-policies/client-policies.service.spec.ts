import { Test, TestingModule } from '@nestjs/testing';
import { ClientPoliciesService } from './client-policies.service';
import { ClientService } from '../../entities/client/client.service';

describe('ClientPoliciesService', () => {
  let service: ClientPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClientPoliciesService,
        {
          provide: ClientService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ClientPoliciesService>(ClientPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
