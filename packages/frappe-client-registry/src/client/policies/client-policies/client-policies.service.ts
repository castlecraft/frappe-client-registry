import { Injectable, BadRequestException } from '@nestjs/common';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ClientService } from '../../entities/client/client.service';
import { CLIENT_ALREADY_EXIST } from '../../../constants/messages';

@Injectable()
export class ClientPoliciesService {
  constructor(private readonly clientService: ClientService) {}

  validateClient(clientPayload) {
    return from(this.clientService.findOne({ name: clientPayload.name })).pipe(
      switchMap(client => {
        if (client) {
          return throwError(new BadRequestException(CLIENT_ALREADY_EXIST));
        }
        return of(true);
      }),
    );
  }
}
