import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateClientCommand } from './update-client.command';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';

@CommandHandler(UpdateClientCommand)
export class UpdateClientHandler
  implements ICommandHandler<UpdateClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientAggregateService,
  ) {}

  async execute(command: UpdateClientCommand) {
    const { clientPayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateClient(clientPayload).toPromise();
    aggregate.commit();
  }
}
