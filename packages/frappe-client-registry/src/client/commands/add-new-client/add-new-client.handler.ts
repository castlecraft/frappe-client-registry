import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { AddNewClientCommand } from './add-new-client.command';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';

@CommandHandler(AddNewClientCommand)
export class AddNewClientHandler
  implements ICommandHandler<AddNewClientCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientAggregateService,
  ) {}

  async execute(command: AddNewClientCommand) {
    const { clientPayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.addNewClient(clientPayload).toPromise();
    aggregate.commit();
  }
}
