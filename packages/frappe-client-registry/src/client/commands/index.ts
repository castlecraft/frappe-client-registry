import { AddNewClientHandler } from './add-new-client/add-new-client.handler';
import { UpdateClientHandler } from './update-client/update-client.handler';
import { RemoveClientHandler } from './remove-client/remove-client.handler';
import { AddNewFrappeInstanceHandler } from './add-new-frappe-instance/add-new-frappe-instance.event';
import { AddFrappeAPIKeyHandler } from './add-frappe-api-key/add-frappe-api-key.handler';
import { UpdateFrappeAPIKeyHandler } from './update-frappe-api-key/update-frappe-api-key.handler';

export const CommandManager = [
  AddNewClientHandler,
  UpdateClientHandler,
  RemoveClientHandler,
  AddNewFrappeInstanceHandler,
  AddFrappeAPIKeyHandler,
  UpdateFrappeAPIKeyHandler,
];
