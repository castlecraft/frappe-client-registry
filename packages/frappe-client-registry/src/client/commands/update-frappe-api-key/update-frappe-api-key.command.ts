import { ICommand } from '@nestjs/cqrs';
import { AddFrappeAPIKeyDto } from '../../../client/entities/client/add-frappe-api-key.dto';

export class UpdateFrappeAPIKeyCommand implements ICommand {
  constructor(public clientPayload: AddFrappeAPIKeyDto) {}
}
