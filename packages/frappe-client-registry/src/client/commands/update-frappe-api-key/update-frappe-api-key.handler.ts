import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';
import { UpdateFrappeAPIKeyCommand } from './update-frappe-api-key.command';

@CommandHandler(UpdateFrappeAPIKeyCommand)
export class UpdateFrappeAPIKeyHandler
  implements ICommandHandler<UpdateFrappeAPIKeyCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ClientAggregateService,
  ) {}

  async execute(command: UpdateFrappeAPIKeyCommand) {
    const { clientPayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateFrappeApiKey(clientPayload).toPromise();
    aggregate.commit();
  }
}
