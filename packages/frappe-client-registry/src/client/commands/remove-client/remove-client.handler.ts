import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveClientCommand } from './remove-client.command';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';

@CommandHandler(RemoveClientCommand)
export class RemoveClientHandler
  implements ICommandHandler<RemoveClientCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: ClientAggregateService,
  ) {}
  async execute(command: RemoveClientCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeClient(uuid);
    aggregate.commit();
  }
}
