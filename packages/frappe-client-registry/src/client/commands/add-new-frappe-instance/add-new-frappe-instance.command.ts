import { ICommand } from '@nestjs/cqrs';
import { AddNewFrappeInstanceDto } from '../../entities/client/add-frappe-instance.dto';

export class AddNewFrappeInstanceCommand implements ICommand {
  constructor(public clientPayload: AddNewFrappeInstanceDto) {}
}
