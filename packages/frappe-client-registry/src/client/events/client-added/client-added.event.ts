import { IEvent } from '@nestjs/cqrs';
import { Client } from '../../entities/client/client.entity';

export class ClientAddedEvent implements IEvent {
  constructor(public client: Client) {}
}
