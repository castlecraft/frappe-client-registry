import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientAddedEvent } from './client-added.event';
import { ClientService } from '../../entities/client/client.service';

@EventsHandler(ClientAddedEvent)
export class ClientAddedHandler implements IEventHandler<ClientAddedEvent> {
  constructor(private readonly addClient: ClientService) {}

  async handle(event: ClientAddedEvent) {
    const { client } = event;
    await this.addClient.create(client);
  }
}
