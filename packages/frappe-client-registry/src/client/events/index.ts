import { ClientAddedHandler } from './client-added/client-added.handler';
import { ClientUpdatedHandler } from './client-updated/client-updated.handler';
import { ClientRemovedHandler } from './client-removed/client-removed.handler';
import { FrappeInstanceAddedHandler } from './frappe-instance-added/frappe-instance-added.handler';
import { FrappeAPIKeyAddedHandler } from './frappe-api-key-added/frappe-api-key-added.handler';
import { FrappeAPIKeyUpdatedHandler } from './frappe-api-key-updated/frappe-api-key-updated.handler';

export const EventManager = [
  ClientAddedHandler,
  ClientUpdatedHandler,
  ClientRemovedHandler,
  FrappeInstanceAddedHandler,
  FrappeAPIKeyAddedHandler,
  FrappeAPIKeyUpdatedHandler,
];
