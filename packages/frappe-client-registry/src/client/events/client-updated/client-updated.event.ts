import { IEvent } from '@nestjs/cqrs';
import { Client } from '../../entities/client/client.entity';

export class ClientUpdatedEvent implements IEvent {
  constructor(public clientPayload: Client) {}
}
