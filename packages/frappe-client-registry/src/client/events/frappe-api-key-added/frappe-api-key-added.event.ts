import { IEvent } from '@nestjs/cqrs';
import { AddFrappeAPIKeyDto } from '../../../client/entities/client/add-frappe-api-key.dto';

export class FrappeAPIKeyAddedEvent implements IEvent {
  constructor(public readonly clientPayload: AddFrappeAPIKeyDto) {}
}
