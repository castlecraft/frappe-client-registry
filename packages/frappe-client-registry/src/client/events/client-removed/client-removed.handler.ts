import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientRemovedEvent } from './client-removed.event';
import { ClientService } from '../../entities/client/client.service';

@EventsHandler(ClientRemovedEvent)
export class ClientRemovedHandler implements IEventHandler<ClientRemovedEvent> {
  constructor(private readonly removeClient: ClientService) {}
  async handle(event: ClientRemovedEvent) {
    const { client } = event;
    await this.removeClient.deleteOne({ uuid: client.uuid });
  }
}
