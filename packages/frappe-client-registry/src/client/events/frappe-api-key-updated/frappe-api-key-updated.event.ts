import { IEvent } from '@nestjs/cqrs';
import { AddFrappeAPIKeyDto } from '../../entities/client/add-frappe-api-key.dto';

export class FrappeAPIKeyUpdatedEvent implements IEvent {
  constructor(public readonly clientPayload: AddFrappeAPIKeyDto) {}
}
