import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ClientService } from '../../entities/client/client.service';
import { UUID } from '../../../constants/app-strings';
import { FrappeAPIKeyUpdatedEvent } from './frappe-api-key-updated.event';

@EventsHandler(FrappeAPIKeyUpdatedEvent)
export class FrappeAPIKeyUpdatedHandler
  implements IEventHandler<FrappeAPIKeyUpdatedEvent> {
  constructor(private readonly clientService: ClientService) {}

  async handle(event: FrappeAPIKeyUpdatedEvent) {
    const { clientPayload } = event;
    const query = {
      'frappeInstance.frappeClientUuid': clientPayload.frappeClientUuid,
    };

    query[UUID] = clientPayload.clientUuid;

    await this.clientService.updateOne(query, {
      $set: { 'frappeInstance.$.frappeApiKey': clientPayload.frappeApiKey },
    });
  }
}
