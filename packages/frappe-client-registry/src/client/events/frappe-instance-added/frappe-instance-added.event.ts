import { IEvent } from '@nestjs/cqrs';
import { AddNewFrappeInstanceDto } from '../../entities/client/add-frappe-instance.dto';

export class FrappeInstanceAddedEvent implements IEvent {
  constructor(public frappeInstance: AddNewFrappeInstanceDto) {}
}
