import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';
import { RetrieveFrappeInstanceQuery } from './retrieve-frappe-instance.query';

@QueryHandler(RetrieveFrappeInstanceQuery)
export class RetrieveFrappeInstanceHandler
  implements IQueryHandler<RetrieveFrappeInstanceQuery> {
  constructor(private manager: ClientAggregateService) {}

  async execute(query: RetrieveFrappeInstanceQuery) {
    const { clientUuid, frappeClientUuid } = query;
    return await this.manager.retrieveFrappeInstance(
      clientUuid,
      frappeClientUuid,
    );
  }
}
