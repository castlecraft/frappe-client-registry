import { RetrieveClientQuery } from './retrieve-client.query';
import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';

@QueryHandler(RetrieveClientQuery)
export class RetrieveClientHandler
  implements IQueryHandler<RetrieveClientQuery> {
  constructor(private manager: ClientAggregateService) {}

  async execute(query: RetrieveClientQuery) {
    const { uuid, clientHttpRequest } = query;
    return await this.manager.retrieveClient(uuid, clientHttpRequest);
  }
}
