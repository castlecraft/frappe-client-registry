import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveClientListQuery } from './retrieve-client-list.query';
import { ClientAggregateService } from '../../aggregates/client-aggregate/client-aggregate.service';

@QueryHandler(RetrieveClientListQuery)
export class RetrieveClientListHandler
  implements IQueryHandler<RetrieveClientListQuery> {
  constructor(private readonly manager: ClientAggregateService) {}
  async execute(query: RetrieveClientListQuery) {
    const { offset, limit, sort, clientHttpRequest, search } = query;
    return await this.manager.getClientList(
      Number(offset),
      Number(limit),
      sort,
      search,
      clientHttpRequest,
    );
  }
}
