import { Module, Global } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CqrsModule } from '@nestjs/cqrs';
import { Client } from './client/client.entity';
import { ClientService } from './client/client.service';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([Client]), CqrsModule],
  providers: [ClientService],
  exports: [ClientService],
})
export class ClientEntitiesModule {}
