import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Client } from './client.entity';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ClientService {
  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: MongoRepository<Client>,
  ) {}

  async create(client: Client) {
    const clientObject = new Client();
    Object.assign(clientObject, client);
    return await clientObject.save();
  }

  async find(query?) {
    return await this.clientRepository.find(query);
  }
  async findOne(query, options?) {
    return await this.clientRepository.findOne(query, options);
  }

  async updateClient(provider: Client) {
    const client = await this.clientRepository.findOne({ uuid: provider.uuid });
    Object.assign(client, provider);
    return client.save();
  }

  async updateOne(query, params) {
    return await this.clientRepository.updateOne(query, params);
  }

  async deleteOne(query, options?) {
    return await this.clientRepository.deleteOne(query, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');

    const searchFilter = this.clientRepository.manager.connection
      .getMetadata(Client)
      .ownColumns.map(column => column.propertyName);

    const $or = searchFilter.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const select: any = [
      '_id',
      'frappeInstance.frappeClientUuid',
      'frappeInstance.clientUuid',
      'frappeInstance.name',
      'name',
      'uuid',
    ];
    const results = await this.clientRepository.find({
      skip,
      take,
      where,
      select,
    });

    return {
      docs: results || [],
      length: await this.clientRepository.count(where),
      offset: skip,
    };
  }

  async aggregate(query) {
    return await this.clientRepository.aggregate(query);
  }

  asyncAggregate(query) {
    return of(this.clientRepository.aggregate(query)).pipe(
      switchMap((aggregateData: any) => {
        return aggregateData.toArray();
      }),
    );
  }
}
