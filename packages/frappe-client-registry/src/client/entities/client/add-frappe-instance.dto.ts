import { IsNotEmpty } from 'class-validator';

export class AddNewFrappeInstanceDto {
  @IsNotEmpty()
  frappeClientUuid: string;

  @IsNotEmpty()
  clientUuid: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  bloomstackBotPassword: string;
}
