import { IsOptional, IsNotEmpty } from 'class-validator';

export class ClientDto {
  @IsOptional()
  uuid: string;

  @IsNotEmpty()
  name: string;
}
