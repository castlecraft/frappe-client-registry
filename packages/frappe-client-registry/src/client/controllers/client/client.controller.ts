import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Param,
  UseGuards,
  Get,
  Query,
  Req,
  UseInterceptors,
  HttpException,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddNewClientCommand } from '../../commands/add-new-client/add-new-client.command';
import { UpdateClientCommand } from '../../commands/update-client/update-client.command';
import { UpdateClientDto } from '../../entities/client/update-client.dto';
import { ClientDto } from '../../entities/client/client.dto';
import { RemoveClientCommand } from '../../commands/remove-client/remove-client.command';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RetrieveClientListQuery } from '../../query/retrieve-client-list/retrieve-client-list.query';
import { RetrieveClientQuery } from '../../query/retrieve-client/retrieve-client.query';
import { RavenInterceptor } from 'nest-raven';
import { AddNewFrappeInstanceDto } from '../../entities/client/add-frappe-instance.dto';
import { AddNewFrappeInstanceCommand } from '../../commands/add-new-frappe-instance/add-new-frappe-instance.command';
import { ADMINISTRATOR } from '../../../constants/app-strings';
import { Roles } from '../../../auth/decorators/roles.decorator';
import { RoleGuard } from '../../../auth/guards/role.guard';
import { AddFrappeAPIKeyCommand } from '../../../client/commands/add-frappe-api-key/add-frappe-api-key.command';
import { AddFrappeAPIKeyDto } from '../../../client/entities/client/add-frappe-api-key.dto';
import { RetrieveFrappeInstanceQuery } from '../../../client/query/retrieve-frappe-instance/retrieve-frappe-instance.query';
import { UpdateFrappeAPIKeyCommand } from '../../../client/commands/update-frappe-api-key/update-frappe-api-key.command';
import { RetrieveFrappeInstanceAsServerQuery } from '../../../client/query/retrieve-frappe-instance-as-server/retrieve-frappe-instance-as-server.query';
import { ServerGuard } from '../../../auth/guards/server.guard';

@Controller('client')
@UseInterceptors(
  new RavenInterceptor({
    filters: [
      {
        type: HttpException,
        filter: (exception: HttpException) => 499 < exception.getStatus(),
      },
    ],
  }),
)
export class ClientController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  addClient(@Body() clientPayload: ClientDto) {
    return this.commandBus.execute(new AddNewClientCommand(clientPayload));
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() clientPayload: UpdateClientDto) {
    return this.commandBus.execute(new UpdateClientCommand(clientPayload));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  removeClient(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveClientCommand(uuid));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveClientListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid, @Req() req) {
    return await this.queryBus.execute(new RetrieveClientQuery(uuid, req));
  }

  @Get('v1/get_frappe_instance')
  @UseGuards(TokenGuard)
  async getFrappeInstance(
    @Query('clientUuid') clientUuid,
    @Query('frappeClientUuid') frappeClientUuid,
  ) {
    return await this.queryBus.execute(
      new RetrieveFrappeInstanceQuery(clientUuid, frappeClientUuid),
    );
  }

  @Get('v1/get_frappe_instance_as_server')
  @UseGuards(TokenGuard, ServerGuard)
  async getFrappeInstanceAsServer(
    @Query('clientUuid') clientUuid,
    @Query('frappeClientUuid') frappeClientUuid,
  ) {
    return await this.queryBus.execute(
      new RetrieveFrappeInstanceAsServerQuery(clientUuid, frappeClientUuid),
    );
  }

  @Post('v1/add_frappe_instance')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  addNewFrappeInstance(@Body() body: AddNewFrappeInstanceDto) {
    return this.commandBus.execute(new AddNewFrappeInstanceCommand(body));
  }

  @Post('v1/add_frappe_api_key')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(TokenGuard, RoleGuard)
  setupFrappeServerApi(@Body() body: AddFrappeAPIKeyDto) {
    return this.commandBus.execute(new AddFrappeAPIKeyCommand(body));
  }

  @Post('v1/update_frappe_api_key')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(TokenGuard, RoleGuard)
  updateFrappeServerApi(@Body() body: AddFrappeAPIKeyDto) {
    return this.commandBus.execute(new UpdateFrappeAPIKeyCommand(body));
  }
}
