import { INestApplication } from '@nestjs/common';
import * as Sentry from '@sentry/node';
import { ConfigService, NODE_ENV, SENTRY_DSN } from './config/config.service';

export function setupSentry(app: INestApplication) {
  const config = app.get<ConfigService>(ConfigService);
  if (config.get(NODE_ENV) === 'production') {
    Sentry.init({ dsn: config.get(SENTRY_DSN) });
  }
}
